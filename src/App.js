import React, { Component } from 'react'
// import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css'
// import { Container, Row, Col, Button, ListGroup, Modal, Form, Alert } from 'react-bootstrap';
import Modal from 'react-modal';
import { FaPlus, FaPencilAlt, FaTrashAlt, FaFile } from "react-icons/fa";

class App extends Component {
  constructor() {
    super()
    this.state = {
      todoList: [],
      todoListFix: [],
      inputTitle: '',
      inputDescription: '',
      inputStatus: 'select',
      addModal: false,
      editModal: false,
      deleteModal: false,
      detailModal: false,
      startDate: '',
      dataCompleted: [],
      dataIncomplete: [],
      tempData: [],
      showAlert: false
    }
  }

  componentDidMount(){
    const urlFetch = fetch('https://897ee1a7-a6ed-46ca-92ca-52a440c60807.mock.pstmn.io/to-do-list')
    urlFetch.then( res => {
      if(res.status === 200)
        return res.json()   
    }).then( resJson => {
       this.setState({
           todoList: resJson
       })
       this.sortingStatus();
    })
  }

  sortingStatus(){
    var dataSekarang = this.state.todoList;

    let dataCompleted = [];
    let dataIncomplete = [];
    dataSekarang.map(function(data, i){
      if(data.status == 1){
        dataCompleted.push(data)
      } else {
        dataIncomplete.push(data)
      }
    })

    dataIncomplete.sort(function(a, b){ 
        return new Date(a.createdAt) - new Date(b.createdAt); 
    });

    dataCompleted.sort(function(a, b){ 
      return new Date(b.createdAt) - new Date(a.createdAt); 
    });

    let dataSekarangFix = [];
    dataIncomplete.map(function(data, i){
      dataSekarangFix.push(data)
    })

    dataCompleted.map(function(data, i){
      dataSekarangFix.push(data)
    })

    this.setState({
      todoListFix: dataSekarangFix
    })

    this.showAddModal(false)
  }

  handleInputChange = e => {
    this.setState({
      inputTitle: e.target.value
    })
  }

  handleDescChange = e => {
    this.setState({
      inputDescription: e.target.value
    })
  }

  handleDropdown = (e) => {
    this.setState({ 
      inputStatus: e.target.value 
    });
  };

  showAddModal(visible){
    this.setState({
      addModal: visible
    })
  }

  addItem(){
    if(this.state.inputTitle !='' && this.state.inputDescription !=''){
      var datanya = this.state.todoList;
      if(datanya !=''){
        var last = datanya.slice(-1)[0]+1;
      } else {
        var last = 1;
      }

      var now     = new Date(); 
      var year    = now.getFullYear();
      var month   = now.getMonth()+1; 
      var day     = now.getDate();
      var hour    = now.getHours();
      var minute  = now.getMinutes();
      var second  = now.getSeconds(); 
      if(month.toString().length == 1) {
            month = '0'+month;
      }
      if(day.toString().length == 1) {
            day = '0'+day;
      }   
      if(hour.toString().length == 1) {
            hour = '0'+hour;
      }
      if(minute.toString().length == 1) {
            minute = '0'+minute;
      }
      if(second.toString().length == 1) {
            second = '0'+second;
      }   
      var dateTime = year+'-'+month+'-'+day+' '+hour+':'+minute;   
      
      let dataBaru = {
        "id": last,
        "title": this.state.inputTitle,
        "description": this.state.inputDescription,
        "status": 0,
        "createdAt": dateTime
      }
      datanya.push(dataBaru);
      
      this.setState({
        inputTitle: '',
        inputDescription: '',
        inputStatus: '',
        showAlert: false
      })

      this.sortingStatus();
    } else {
      this.setState({
        showAlert: true
      })
    }
  }

  detailData(i){
    var data = this.state.todoListFix;
    var getData = data.find(({id}) => id === i );

    this.setState({
      tempData: getData,
      detailModal: true
    },
      function() { 
        console.log(this.state.tempData) 
        console.log(this.state.inputTitle)
        console.log(this.state.inputDescription)
        console.log(this.state.inputStatus)
      }
    )
    
  }

  editData(i){
    var data = this.state.todoListFix;
    var getData = data.find(({id}) => id === i );

    this.setState({
      tempData: getData,
      editModal: true,
      inputTitle: getData.title,
      inputDescription: getData.description,
      inputStatus: getData.status
    },
      function() { 
        console.log(this.state.tempData) 
        console.log(this.state.inputTitle)
        console.log(this.state.inputDescription)
        console.log(this.state.inputStatus)
      }
    )
    
  }

  doEdit(id){
    if(this.state.inputTitle !='' && this.state.inputDescription !='' && this.state.inputStatus !='select'){
      var datanya = this.state.todoList;
      var objIndex = datanya.findIndex((obj => obj.id == id));
      
      let dataBaru = {
        "id": id,
        "title": this.state.inputTitle,
        "description": this.state.inputDescription,
        "status": this.state.inputStatus,
        "createdAt": this.state.tempData.createdAt
      }

      datanya[objIndex] = dataBaru;

      this.setState({
        todoList: datanya,
        editModal: false,
        inputTitle: '',
        inputDescription: '',
        inputStatus: '',
        showAlert: false
      },
        function() { this.sortingStatus(); }
      )
    } else {
      this.setState({
        showAlert: true
      })
    }
  }

  deleteData(i){
    var data = this.state.todoListFix;
    var getData = data.find(({id}) => id === i );

    this.setState({
      tempData: getData,
      deleteModal: true
    },
      function() { console.log(this.state.tempData) }
    ) 
  }

  doDelete(id){
    var datanya = this.state.todoList;
    var fix = datanya.filter((obj => obj.id !== id));

    this.setState({
      todoList: fix,
      deleteModal: false,
      inputTitle: '',
      inputDescription: '',
      inputStatus: ''
    },
      function() { this.sortingStatus(); }
    )
  }

  displayList() { 
    if (this.state.todoListFix.length > 0) {
      return this.state.todoListFix.map(function(data, i){
        return ( 
          <div class="item" key={i}>
            <p class="judul">
              {data.title} 
              <span class="keterangan"><b>{data.createdAt}</b></span>
            </p>
            {data.status == 1 ? (
              <span class="complete">Completed</span>
            ) : (
              <span class="incomplete">Incomplete</span>
            )}
            {data.status == 1 ? (
              <div>
                <a class="btn-edit" title="Edit" onClick={() => this.editData(data.id)} key={i}><FaPencilAlt/></a>
                <a class="btn-detail" title="Detail" onClick={() => this.detailData(data.id)} key={'detail_' + i}><FaFile/></a>
              </div>
            ) : (
              <div>
                <a class="btn-delete" title="Delete" key={'data_'+i} onClick={() => this.deleteData(data.id)}><center><FaTrashAlt/></center></a>
                <a class="btn-edit" title="Edit" key={i} onClick={() => this.editData(data.id)}><FaPencilAlt/></a>
                <a class="btn-detail" title="Detail" onClick={() => this.detailData(data.id)} key={'detail_' + i}><FaFile/></a>  
              </div>
            )}
          </div>
        );
      }, this)
    } else {
      return (
        <h6 class="no-data">No Data </h6>
      )
    }
  }

  render() {
    return (
      <center>
        <div class="container">
          <h2 class="title">TODO <span style={{ color: '#555cff' }}>APPLICATION</span></h2>
          <a class="btn-add" onClick={() => this.showAddModal(true)}><b>ADD TASK</b></a>
          {this.displayList()}

          <Modal
            isOpen={this.state.addModal}
            onRequestClose={() => this.showAddModal(false)}
            className="Modal"
            overlayClassName="Overlay"
            contentLabel="Example Modal"
          >
            <a class="close-modal" onClick={() => this.showAddModal(false)}>x</a>
            <div class="modal-judul">ADD <span style={{ color: '#555cff' }}>NEW TASK</span></div>
            <form class="form-modal">
              <input class="form-input" type="text" placeholder="Enter title" onChange={this.handleInputChange} />
              
              <br></br>

              <input class="form-input form-bawah" type="text" placeholder="Enter description" onChange={this.handleDescChange} />
              <br></br>
              <a class="form-button" onClick={() => this.addItem()}>
                Save
              </a>
            </form>
          </Modal>

          <Modal
            isOpen={this.state.editModal}
            onRequestClose={() => this.setState({ editModal: false })}
            className="Modal"
            overlayClassName="Overlay"
            contentLabel="Example Modal"
          >
            <a class="close-modal" onClick={() => this.setState({ editModal: false })}>x</a>
            <div class="modal-judul">EDIT <span style={{ color: '#555cff' }}>TASK</span></div>
            <form class="form-modal">
              <input class="form-input" type="text" placeholder="Enter title" onChange={this.handleInputChange} defaultValue={this.state.tempData.title}/>
              
              <br></br>

              <input class="form-input" type="text" placeholder="Enter description" onChange={this.handleDescChange} defaultValue={this.state.tempData.description}/>
              <br></br>

              <select class="form-select form-bawah" value={this.state.tempData.status} onChange={this.handleDropdown}>
                <option style={{ backgroundColor: '#555cff' }} value="select">Select Status</option>
                <option style={{ backgroundColor: '#555cff' }} value="0">Incomplete</option>
                <option style={{ backgroundColor: '#555cff' }} value="1">Completed</option>
              </select>
              <br></br>

              <a class="form-button" onClick={() => this.doEdit(this.state.tempData.id)}>
                Save
              </a>
            </form>
          </Modal>

          <Modal
            isOpen={this.state.detailModal}
            onRequestClose={() => this.setState({ detailModal: false })}
            className="Modal"
            overlayClassName="Overlay"
            contentLabel="Example Modal"
          >
            <a class="close-modal" onClick={() => this.setState({ detailModal: false })}>x</a>
            <div class="modal-judul">TASK <span style={{ color: '#555cff' }}>DETAIL</span></div>
            <p style={{ color: 'white', fontSize: 12, marginTop: 25 }}>Title</p>
            <p style={{ color: 'white', fontSize: 14, marginTop: -10, fontWeight: 'bold' }}>{this.state.tempData.title}</p>

            <p style={{ color: 'white', fontSize: 12, marginTop: 10 }}>Description</p>
            <p style={{ color: 'white', fontSize: 14, marginTop: -10, fontWeight: 'bold' }}>{this.state.tempData.description}</p>

            <p style={{ color: 'white', fontSize: 12, marginTop: 10 }}>Time</p>
            <p style={{ color: 'white', fontSize: 14, marginTop: -10, fontWeight: 'bold' }}>{this.state.tempData.createdAt}</p>

            <p style={{ color: 'white', fontSize: 12, marginTop: 10, marginBottom: 5 }}>Task Status</p>
            {this.state.tempData.status == 1 ? (
              <span class="complete">Completed</span>
            ) : (
              <span class="incomplete">Incomplete</span>
            )}
          </Modal>

          <Modal
            isOpen={this.state.deleteModal}
            onRequestClose={() => this.setState({ deleteModal: false })}
            className="ModalDelete"
            overlayClassName="Overlay"
            contentLabel="Example Modal"
          >
            <a class="close-modal" onClick={() => this.setState({ deleteModal: false })}>x</a>
            <div class="modal-judul">DELETE <span style={{ color: '#555cff' }}>TASK</span></div>
            <p style={{ color: 'white', fontSize: 13, marginTop: 20, marginBottom: 20 }}>Are you sure want to delete this task?</p>
            <a class="form-button" onClick={() => this.doDelete(this.state.tempData.id)}>
              Yes
            </a>
            <a class="form-button-cancel" onClick={() => this.setState({ deleteModal: false })}>
              No
            </a>
            
          </Modal>
        </div>
      </center>
    )
  }
}
export default App